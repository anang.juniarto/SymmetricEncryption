﻿using SymmetricEncryptionExample.Security;
using System;
using System.Security.Cryptography;
using System.Text;

namespace SymmetricEncryptionExample
{
    class Program
    {
        static void Main(string[] args)
        {
            var text = "I have a big dog. You've got a cat. We all love animals!";

            Start:
            Console.Write("Input Text Encrypt : ");
            text = Console.ReadLine();

            Console.WriteLine("-- Encrypt Decrypt symmetric --");
            Console.WriteLine("");

            CryptographySymmetricEncryption sytmmetric = new CryptographySymmetricEncryption();
            var (Key, IVBase64) = sytmmetric.initSymmetricEncryptionKeyIV();

            var encryptedText = sytmmetric.symmetricEncryptString(text, IVBase64, Key);

            Console.WriteLine("-- Key --");
            Console.WriteLine(Key);
            Console.WriteLine("-- IVBase64 --");
            Console.WriteLine(IVBase64);

            Console.WriteLine("");
            Console.WriteLine("-- Encrypted Text --");
            Console.WriteLine(encryptedText);

            var decryptedText = sytmmetric.symmetricDecryptString(encryptedText, IVBase64, Key);

            Console.WriteLine("-- Decrypted Text --");
            Console.WriteLine(decryptedText);

            goto Start;
        }
    }
}
