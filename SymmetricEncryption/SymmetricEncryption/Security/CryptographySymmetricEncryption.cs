﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace SymmetricEncryptionExample.Security
{
    public class CryptographySymmetricEncryption
    {

        //private const string STRING_KEY_DATABASE = "riPMYDxHWBdCa1RhF/wPbsYHvGTB2czaMDWk7SGyHAg=";

        private byte[] generateRandomBytes(int length)
        {
            var byteArray = new byte[length];
            RandomNumberGenerator.Fill(byteArray);
            return byteArray;
        }

        public string getEncodedRandomString(int length)
        {
            return Convert.ToBase64String(generateRandomBytes(length));
        }

        private Aes createCipher(string keyBase64)
        {
            // Default values: Keysize 256, Padding PKC27
            Aes cipher = Aes.Create();
            cipher.Mode = CipherMode.CBC;  // Ensure the integrity of the ciphertext if using CBC

            cipher.Padding = PaddingMode.PKCS7;
            cipher.Key = Convert.FromBase64String(keyBase64);

            return cipher;
        }

        public (string key, string Hash) initSymmetricEncryptionKeyIV()
        {
            var key = getEncodedRandomString(32); // 256
            //var key = STRING_KEY_DATABASE;
            Aes cipher = createCipher(key);
            var IVBase64 = Convert.ToBase64String(cipher.IV);
            return (key, IVBase64);
        }

        public string symmetricEncryptString(string textToEncrypted, string IV, string key)
        {
            Aes cipher = createCipher(key);
            cipher.IV = Convert.FromBase64String(IV);

            ICryptoTransform cryptTransform = cipher.CreateEncryptor();
            byte[] plaintext = Encoding.UTF8.GetBytes(textToEncrypted);
            byte[] cipherText = cryptTransform.TransformFinalBlock(plaintext, 0, plaintext.Length);

            return Convert.ToBase64String(cipherText);
        }
        
        public string symmetricDecryptString(string encryptedText, string IV, string key)
        {
            Aes cipher = createCipher(key);
            cipher.IV = Convert.FromBase64String(IV);

            ICryptoTransform cryptTransform = cipher.CreateDecryptor();
            byte[] encryptedBytes = Convert.FromBase64String(encryptedText);
            byte[] plainBytes = cryptTransform.TransformFinalBlock(encryptedBytes, 0, encryptedBytes.Length);

            return Encoding.UTF8.GetString(plainBytes);
        }
    }
}
